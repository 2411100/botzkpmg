<?php
namespace Mappers;

use Models\OpeningTimes;
use PDO, PDOException;

class OpeningTimesMapper {

  protected $conn;
  protected $tableName = "openingTimes";

  public function __construct($dbConnection) {
    $this->conn = $dbConnection;
  }

  public function getOpeningTimes() {

    $sqlQuery = "SELECT * FROM $this->tableName";
    $stmt = $this->conn->prepare($sqlQuery);
    $stmt->execute();

    $openingTimes = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $numberOfOpeningTimes = count($openingTimes);

    if($numberOfOpeningTimes > 0) {

      return $openingTimes;
    }
    else {
      return array("message" => "No opening times found.");
    }
  }

  public function updateOpeningTimes(OpeningTimes $newTime) {

    $sqlQuery = "UPDATE $this->tableName SET open=:newOpen, close=:newClose WHERE id=:id";
    $stmt = $this->conn->prepare($sqlQuery);

    $stmt->bindParam(':newOpen', $newTime->open);
    $stmt->bindParam(':newClose', $newTime->close);
    $stmt->bindParam(':id', $newTime->id);

    return $stmt->execute();

  }
}
