<?php

namespace Actions;

include_once dirName(__DIR__) . '/autoload.php';

use Db\Db;
use Mappers\OpeningTimesMapper;
use Models\OpeningTimes;
use Models\Response;

// Response Headers
// Allow requests from any origin
header("Access-Control-Allow-Origin: *");
// Allow only requests that have been made using the POST method
header("Access-Control-Allow-Methods: POST");
// Inform requester that response is in form of json (utf-8 charset)
header("Content-Type: application/json; charset=UTF-8");


// The if statament ensures that the Access-Control-Allow-Methods has been truly adhered to
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  // instantiate database and product object
  $db = Db::getInstance();

  // initialize mapper object
  $openingTimesMapper = new OpeningTimesMapper($db);

  // Set the filters array for the data coming in
  $filters = array(
    'id'=>FILTER_SANITIZE_NUMBER_INT,
    'day'=>FILTER_SANITIZE_STRING,
    'open'=>FILTER_SANITIZE_STRING,
    'close'=>FILTER_SANITIZE_STRING,
  );

  // Collect the data sent fromt the front end into an object
  $data = json_decode(file_get_contents("php://input"));

  // Create an array with field values set to null to pass help create newFilteredTime instance
  $nullData = array(
    'id'=>null,
    'day'=>null,
    'open'=>null,
    'close'=>null
  );

  // Loop through the JSON data sent, sanitize and then update the opening times
  // Break out of the loop and return a false if any query to database fails

  foreach($data as $openingTime) {
    $filteredNewTime = new OpeningTimes($nullData);
    foreach($openingTime as $key=>$value) {
      $filteredNewTime->$key = filter_var($value, $filters[$key]);
    }
    $result = $openingTimesMapper->updateOpeningTimes($filteredNewTime);
    if(!$result) {
      break;
    }
  }

  // Return the correct response depending on whether the queries were a success or not.
  if($result) {
    echo json_encode(new Response(1, "Data Saved"));
  }
  else {
    echo json_encode(new Response(0, "Something went wrong, data not saved"));
  }
}
else {
  echo json_encode(new Response(0, "The server rejected the request"));
}
