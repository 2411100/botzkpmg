import React from 'react';

import FilmForm from './FilmForm';

const AddFilm = (props) => {
  return (
  <div>
    <h1>Add a film</h1>
    <p>Use the form below to fill in the details</p>
    <FilmForm updateFilm={props.updateFilm} modalText={props.modalText} closeModal={props.closeModal} />
  </div>
);
};

export default AddFilm;
