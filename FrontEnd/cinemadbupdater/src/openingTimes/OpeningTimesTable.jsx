import React from 'react';

import OpeningTimesHeaderRow from './OpeningTimesHeaderRow';
import OpeningTimesDayRow from './OpeningTimesDayRow';
import SaveButton from '../films/SaveButton';

import { urls, openingUrls } from '../js/constants';

export default class OpeningTimesTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openingTimes: null,
    };

    this.updateTimes = this.updateTimes.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
  }

  componentDidMount() {
    //let openingTimes = this.state.openingTimes;
    //let url = `${urls.base1}/${this.props.mode}/${openingUrls.get}`;
    let url = `${urls.base1}/${openingUrls.get}`;
    fetch(url/*, {
      mode: "cors",
      method: "GET"
    }*/)
      .then(response => response.json())
      .then(openingTimes => {
        this.setState({
          openingTimes
        });
      });
  }

  updateTimes(event) {
    let openingTimes = this.state.openingTimes;
    let dayToFind = (event.target.id).split(/day/)[0];
    let dayToModify = openingTimes.find(times => times.day.startsWith(dayToFind));
    let timeToFind = (event.target.id).split(/day/)[1];
    dayToModify[timeToFind] = event.target.innerText;
    this.setState(openingTimes);
  }

  saveChanges() {
    let openingTimes = this.state.openingTimes;
    let url = `${urls.base1}/${openingUrls.update}`;
    fetch(url, {
      body: JSON.stringify(openingTimes),
      mode: 'cors',
      method: 'POST'
    })
      .then(response => response.json())
      .then(result => console.log(result.message));
  }

  render() {
    return (
      <div>
        <table className="hover">
          <OpeningTimesHeaderRow />
          <tbody>
            {(this.state.openingTimes !== null) ? this.state.openingTimes.map((opening, i) => (
              <OpeningTimesDayRow
                key={i}
                day={opening.day}
                open={opening.open}
                close={opening.close}
                handleChange={this.updateTimes}
              />)) : false}
          </tbody>
        </table>
        <SaveButton handleClick={this.saveChanges} />
      </div>
    )
  }
}
