export const urls = {
    base1: 'http://34.244.226.177/src/ActionPaths',
};

export const openingUrls = {
    get: 'getOpeningTimes.php',
    update: 'updateOpeningTimes.php'
};

export const filmUrls = {
    get: 'getFilms.php',
    update: 'updateFilm.php',
    add: 'addFilm.php'
};
