import React from 'react';
import { Link } from 'react-router-dom';

const HeaderNav = () => (
  <div className="top-bar-right" id="menu">
    <h6 className="hide">Site Navigation</h6>
    <ul className="vertical medium-horizontal menu">
      <li><Link to="/" className="pageLink">Home</Link></li>
      <li><Link to="/filmActions/showFilms" className="pageLink">Show Films</Link></li>
      <li><Link to="/filmActions/addFilm" className="pageLink">Add Film</Link></li>
      <li><Link to="/showOpeningTimes" className="pageLink">Show Opening Times</Link></li>
    </ul>
  </div>
);

export default HeaderNav;
