import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './Home';
import FilmHome from '../films/FilmHome';
import OpeningTimes from '../openingTimes/OpeningTimes';

const Content = (props) => {
  return (
    <main className="row">
      {/* <div hidden={props.mode}>Please select whether you are running the &nbsp;
          <input type="radio" name="mode" value="Starter" onChange={props.setMode} /> Starter or &nbsp;
          <input type="radio" name="mode" value="Solution" onChange={props.setMode} /> Solution Project
        </div> */}
      <Switch>
        <Route exact path="/" render={() => <Home mode={props.mode} />} />
        <Route
          path="/filmActions"
          render={() => <FilmHome />}
        />
        {/* <Route exact path="/showOpeningTimes" render={() => <OpeningTimes mode={props.mode} />} /> */}
        <Route
          exact
          path="/showOpeningTimes"
          render={() => <OpeningTimes />}
        />
      </Switch>
    </main>
  );
}

export default Content;