<?php
namespace Tests\Mappers;

use Tests\DbOperations\DatabaseTestCase;
use Tests\DbOperations\ArrayDataSet;

use Mappers\OpeningTimesMapper;
use Models\OpeningTimes;
use Db\Db;

class OpeningTimesTest extends DatabaseTestCase {

  public function getDataSet() {

    return $this->createFlatXMLDataSet(__DIR__.'/../../Fixtures/openings-test.xml');
  }

  public function testGetAllOpeningTimesHasThreeResults() {

    $openingTimesMapper = new OpeningTimesMapper(Db::getInstance());

    $openingTimes = $openingTimesMapper->getOpeningTimes();

    $numberOfOpeningTimes = count($openingTimes);

    $testResult = $this->getConnection()->getRowCount('openingTimes');

    $this->assertEquals($testResult, $numberOfOpeningTimes);
  }

  public function testGetAllOpeningTimesResults() {

    $openingTimesMapper = new OpeningTimesMapper(Db::getInstance());

    $openingTimes = $openingTimesMapper->getOpeningTimes();

    $testTable = $this->getConnection()->createQueryTable('openingTimes', 'SELECT * FROM openingTimes');

    $rowCounter = 0;

    foreach($openingTimes as $time) {

      $testRow = $testTable->getRow($rowCounter++);

      $testResult = ($time === $testRow);

      $this->assertTrue($testResult);
    }
  }

  public function testUpdateOpeningTimes() {
    
    $newOpeningTime = "12:05pm";
    $newClosingTime = "11:35pm";

    $openingTimesMapper = new OpeningTimesMapper(Db::getInstance());

    // Get the current table so that modifications can be made to the array before sending it back
    $openingTimes = new OpeningTimes($openingTimesMapper->getOpeningTimes());

    // Loop through the opening times and change open in row 1, close in row 2 and nothing in row 3
    foreach($openingTimes as $key => $value) {
      if($value['id'] == 1) {
        $openingTimes[$key]['open'] = $newOpeningTime;
      }
      if($value['id'] == 2) {
        $openingTimes[$key]['close'] = $newClosingTime;
      }
    }

    $openingTimesMapper->updateOpeningTimes($openingTimes);

    $newOpeningTimes = $openingTimesMapper->getOpeningTimes();
    $testTable = $this->getConnection()->createQueryTable('openingTimes', "SELECT * FROM openingTimes");

    $rowCounter = 0;

    foreach($newOpeningTimes as $time) {
      $testRow = $testTable->getRow($rowCounter++);

      $testResult = ($time === $testRow);

      $this->assertTrue($testResult);

    }
  }
}
